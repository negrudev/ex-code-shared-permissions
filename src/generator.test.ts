import { createPermissionPlan, createPermissionSet } from './generator';

describe('createPermissionSet', () => {
  it('should generate a resource specific CRUD permission set with a certain ID suffix', () => {
    const permissionSet = createPermissionSet(1, {});

    expect(permissionSet).toEqual({
      create: { id: 'c-1', config: {} },
      read: { id: 'r-1', config: {} },
      update: { id: 'u-1', config: {} },
      delete: { id: 'd-1', config: {} }
    });
  });

  it('should generate a unique permission ID if provided user input suffix is unique', () => {
    const permissionSet = createPermissionSet(1, {});
    const permissionSetWithSameIdSuffix = createPermissionSet(1, {});
    const permissionSetWithDifferentIdSuffix = createPermissionSet(2, {});

    expect(permissionSet).toEqual(permissionSetWithSameIdSuffix);
    expect(permissionSet).not.toEqual(permissionSetWithDifferentIdSuffix);
  });

  //  Use case e.g.: a permission to access non-personal projects must have associations to the projects IDs it needs access to
  it('should accept configuration specific to a permission set', () => {
    const permissionSetWithAssociatedResource = createPermissionSet(1, { hasResource: true });

    expect(permissionSetWithAssociatedResource).toEqual({
      create: { id: 'c-1', config: { hasResource: true } },
      read: { id: 'r-1', config: { hasResource: true } },
      update: { id: 'u-1', config: { hasResource: true } },
      delete: { id: 'd-1', config: { hasResource: true } }
    });
  });
});

describe('createPermissionPlan', () => {
  const permissionSet = createPermissionSet(1, {});
  const permissionSetWithAssociatedResource = createPermissionSet(2, { hasResource: true });

  // Use case e.g.: generate a permission plan/suite for a certain user role
  it('should generate a permission plan with certain permissions', () => {
    const permissionPlanWithExtractedPermissions = createPermissionPlan([
      permissionSet.create,
      permissionSet.read
    ]);

    expect(permissionPlanWithExtractedPermissions).toEqual({
      'c-1': {},
      'r-1': {}
    });

    const permissionPlanWithAllPermissions = createPermissionPlan([
      ...Object.values(permissionSet)
    ]);

    expect(permissionPlanWithAllPermissions).toEqual({
      'c-1': {},
      'r-1': {},
      'u-1': {},
      'd-1': {}
    });

    const permissionPlanWithAssociatedResourcePermission = createPermissionPlan([
      ...Object.values(permissionSetWithAssociatedResource)
    ]);

    expect(permissionPlanWithAssociatedResourcePermission).toEqual({
      'c-2': { hasResource: true },
      'r-2': { hasResource: true },
      'u-2': { hasResource: true },
      'd-2': { hasResource: true }
    });

    const permissionPlanMixed = createPermissionPlan([
      ...Object.values(permissionSet),
      ...Object.values(permissionSetWithAssociatedResource)
    ]);

    expect(permissionPlanMixed).toEqual({
      'c-1': {},
      'r-1': {},
      'u-1': {},
      'd-1': {},
      'c-2': { hasResource: true },
      'r-2': { hasResource: true },
      'u-2': { hasResource: true },
      'd-2': { hasResource: true }
    });
  });

  it('should accept configuration specific to a permission plan', () => {
    const permissionPlanWithLimit = createPermissionPlan([...Object.values(permissionSet)], {
      [permissionSet.create.id]: { limit: 5 }
    });

    expect(permissionPlanWithLimit).toEqual({
      'c-1': { limit: 5 },
      'r-1': {},
      'u-1': {},
      'd-1': {}
    });

    const permissionPlanWithResourceAndLimit = createPermissionPlan(
      [...Object.values(permissionSetWithAssociatedResource)],
      {
        [permissionSetWithAssociatedResource.create.id]: { limit: 5 }
      }
    );

    expect(permissionPlanWithResourceAndLimit).toEqual({
      'c-2': { hasResource: true, limit: 5 },
      'r-2': { hasResource: true },
      'u-2': { hasResource: true },
      'd-2': { hasResource: true }
    });
  });
});
