import { ExpectedPermissionContext, PermissionContext } from './api-model/context';
import { Permission, PermissionPlan, PId, ResourceId } from './api-model/permission';

export interface UserPermissionsConfig {
  resources?: {
    [permission: PId]: ResourceId[];
  };
}

export const createResourcePermissionValidator = <Plan extends PermissionPlan>(
  permissionPlan: Plan,
  userPermissionConfig: UserPermissionsConfig
) => {
  const validatePermission = <P extends Permission, PContext extends ExpectedPermissionContext<Plan, P['id']>>(
    permission: P,
    permissionContext: keyof Plan[P['id']] extends never ? undefined : PContext
  ): boolean => {
    const context = permissionContext as PermissionContext | undefined;
    const permissionId = permission.id;

    const hasPermission = !!permissionPlan[permissionId];
    const permissionConfig = permissionPlan[permissionId];

    const currentPermissionCountLimit = permissionConfig?.limit;
    const hasCountUnderLimit = currentPermissionCountLimit
      ? typeof context?.count === 'number'
        ? context.count < currentPermissionCountLimit
        : false
      : true;

    const currentPermissionRequiredResource = userPermissionConfig.resources?.[permissionId as string];
    const hasRequiredResourceAccess = permissionConfig?.hasResource
      ? permissionConfig.hasResource && currentPermissionRequiredResource
        ? context?.resource
          ? currentPermissionRequiredResource.includes(context.resource)
          : false
        : false
      : true;

    const isPermissionGranted = hasPermission && hasCountUnderLimit && hasRequiredResourceAccess;

    return isPermissionGranted;
  };

  return validatePermission;
};
