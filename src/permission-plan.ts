import { permissionPlans, PermissionPlans } from './permissions/plans';

type UnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends (k: infer I) => void ? I : never;

export type MergedPermissionPlans = UnionToIntersection<typeof permissionPlans[PermissionPlans]>;

export const getPermissionPlan = (plan: PermissionPlans): MergedPermissionPlans | undefined => {
  return permissionPlans[plan] as MergedPermissionPlans | undefined;
};
