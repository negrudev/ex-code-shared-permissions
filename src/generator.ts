import { Permission, PermissionPlan, PermissionPlanConfig } from './api-model/permission';

export const createPermissionSet = <T extends number, C extends Permission['config']>(
  id: T,
  config: C
) => {
  return {
    create: { id: `c-${id}`, config },
    read: { id: `r-${id}`, config },
    update: { id: `u-${id}`, config },
    delete: { id: `d-${id}`, config }
  } as const;
};

export const createPermissionPlan = <
  PList extends Readonly<Permission[]>,
  // eslint-disable-next-line @typescript-eslint/ban-types
  Config extends PermissionPlanConfig = {}
>(
  permissions: PList,
  config?: Config
): PermissionPlan<PList, Config> => {
  return permissions.reduce(
    (result, permission) => ({
      ...result,
      [permission.id]: { ...permission.config, ...config?.[permission.id] }
    }),
    {} as PermissionPlan<PList, Config>
  );
};
