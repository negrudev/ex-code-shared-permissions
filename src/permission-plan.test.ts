import { getPermissionPlan } from './permission-plan';
import { PermissionPlans, permissionPlans } from './permissions/plans';

describe('getPermissionPlan', () => {
  it('should return the corresponding permission plan object', () => {
    expect(getPermissionPlan(PermissionPlans.IndividualFree)).toBe(permissionPlans['individualFree']);
    expect(getPermissionPlan(PermissionPlans.IndividualBasic)).toBe(permissionPlans['individualBasic']);
    expect(getPermissionPlan(PermissionPlans.IndividualPro)).toBe(permissionPlans['individualPro']);
    expect(getPermissionPlan(PermissionPlans.Internal)).toBe(permissionPlans['internal']);

    // Boundary assertion: unrecognized resource ID
    expect(getPermissionPlan(null as never)).toBe(undefined);
    expect(getPermissionPlan(undefined as never)).toBe(undefined);
    expect(getPermissionPlan('unknown type' as never)).toBe(undefined);
  });
});
