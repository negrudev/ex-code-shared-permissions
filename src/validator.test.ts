import { createPermissionPlan, createPermissionSet } from './generator';
import { createResourcePermissionValidator } from './validator';

describe('createResourcePermissionValidator', () => {
  const permissionSet = createPermissionSet(1, {});
  const permissionSetWithResource = createPermissionSet(2, { hasResource: true });
  const permissionSetNotInPlan = createPermissionSet(3, {});

  it('should generate a permission validator based on a certain permission plan', () => {
    const permissionPlan = createPermissionPlan([...Object.values(permissionSet)]);

    const validatePermission = createResourcePermissionValidator(permissionPlan, {});

    expect(validatePermission(permissionSet.create, undefined)).toEqual(true);
    expect(validatePermission(permissionSet.read, undefined)).toEqual(true);
    expect(validatePermission(permissionSet.update, undefined)).toEqual(true);
    expect(validatePermission(permissionSet.delete, undefined)).toEqual(true);

    expect(validatePermission(permissionSetNotInPlan.create, {} as never)).toEqual(false);
    expect(validatePermission(permissionSetNotInPlan.read, {} as never)).toEqual(false);
    expect(validatePermission(permissionSetNotInPlan.update, {} as never)).toEqual(false);
    expect(validatePermission(permissionSetNotInPlan.delete, {} as never)).toEqual(false);
  });

  describe("should request a permission context based on each permission's requirements defined in the permission plan", () => {
    it('resource requirement', () => {
      const permissionPlan = createPermissionPlan([...Object.values(permissionSetWithResource)]);

      const validatePermission = createResourcePermissionValidator(permissionPlan, {
        resources: {
          [permissionSetWithResource.create.id]: ['resource_id_1']
        }
      });

      expect(validatePermission(permissionSetWithResource.create, { resource: 'resource_id_1' })).toEqual(true);
      expect(validatePermission(permissionSetWithResource.read, { resource: 'resource_id_1' })).toEqual(false);

      // Boundary assertion: empty resource ID
      expect(validatePermission(permissionSetWithResource.create, { resource: '' })).toEqual(false);
      expect(validatePermission(permissionSetWithResource.read, { resource: '' })).toEqual(false);

      // Boundary assertion: no config values
      expect(validatePermission(permissionSetWithResource.create, {} as never)).toEqual(false);
      expect(validatePermission(permissionSetWithResource.create, undefined as never)).toEqual(false);
      expect(validatePermission(permissionSetWithResource.create, null as never)).toEqual(false);
    });

    it('limit requirement', () => {
      const permissionPlanWithLimit = createPermissionPlan(
        [...Object.values(permissionSet), ...Object.values(permissionSetWithResource)],
        {
          [permissionSet.create.id]: { limit: 5 }
        }
      );

      const validatorPlanWithLimit = createResourcePermissionValidator(permissionPlanWithLimit, {});

      expect(validatorPlanWithLimit(permissionSet.create, { count: 1 })).toEqual(true);
      expect(validatorPlanWithLimit(permissionSet.create, { count: 4 })).toEqual(true);
      expect(validatorPlanWithLimit(permissionSet.create, { count: 5 })).toEqual(false);

      // Boundary assertion: 0 count
      expect(validatorPlanWithLimit(permissionSet.create, { count: 0 })).toEqual(true);

      // Boundary assertion: empty count for expected count but not received
      expect(validatorPlanWithLimit(permissionSet.create, {} as never)).toEqual(false);
      expect(validatorPlanWithLimit(permissionSet.create, undefined as never)).toEqual(false);
      expect(validatorPlanWithLimit(permissionSet.create, null as never)).toEqual(false);

      // Boundary assertion: empty count for non-required count and not received
      expect(validatorPlanWithLimit(permissionSet.read, {} as never)).toEqual(true);
      expect(validatorPlanWithLimit(permissionSet.read, undefined as never)).toEqual(true);
      expect(validatorPlanWithLimit(permissionSet.read, null as never)).toEqual(true);

      // Boundary assertion: count for non-required count but received
      expect(validatorPlanWithLimit(permissionSet.read, { count: 1 } as never)).toEqual(true);
    });

    it('mixed requirement', () => {
      const permissionPlanWithMixedRequirements = createPermissionPlan(
        [...Object.values(permissionSet), ...Object.values(permissionSetWithResource)],
        {
          [permissionSet.create.id]: { limit: 5 },
          [permissionSetWithResource.create.id]: { limit: 5 }
        }
      );

      const validatePermission = createResourcePermissionValidator(permissionPlanWithMixedRequirements, {
        resources: {
          [permissionSetWithResource.create.id]: ['resource_id_1']
        }
      });

      // Valid limit & Valid resource
      expect(validatePermission(permissionSet.create, { count: 1 })).toEqual(true);
      expect(validatePermission(permissionSet.create, { count: 4 })).toEqual(true);
      expect(validatePermission(permissionSet.read, undefined)).toEqual(true);
      expect(validatePermission(permissionSetWithResource.create, { resource: 'resource_id_1', count: 1 })).toEqual(true);
      expect(validatePermission(permissionSetWithResource.create, { resource: 'resource_id_1', count: 4 })).toEqual(true);

      // Invalid limit & Valid resource
      expect(validatePermission(permissionSet.create, { count: 5 })).toEqual(false);
      expect(validatePermission(permissionSetWithResource.create, { resource: 'resource_id_1', count: 5 })).toEqual(false);

      // Invalid limit & Invalid resource
      expect(validatePermission(permissionSetWithResource.create, { resource: 'resource_id_2', count: 6 })).toEqual(false);

      // Valid limit & Invalid resource
      expect(validatePermission(permissionSetWithResource.read, { resource: 'resource_id_1', count: 1 })).toEqual(false);
    });
  });
});
