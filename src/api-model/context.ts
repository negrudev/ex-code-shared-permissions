import { PermissionPlan, ResourceId } from './permission';

export interface PermissionContext {
  resource?: ResourceId;
  count?: number;
}

export type ExpectedPermissionContext<Plan extends PermissionPlan, PId extends keyof Plan> =
  // eslint-disable-next-line @typescript-eslint/ban-types
  (Plan[PId] extends { limit: number } ? { count: number } : {}) &
    // eslint-disable-next-line @typescript-eslint/ban-types
    (Plan[PId] extends { hasResource: boolean } ? { resource: ResourceId } : {});
