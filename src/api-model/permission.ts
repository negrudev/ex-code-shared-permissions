export type PId = string;
export type ResourceId = string;

export interface Permission {
  id: PId;
  config?: {
    hasResource?: boolean;
  };
}

export interface PermissionSet {
  create: Permission;
  read: Permission;
  update: Permission;
  delete: Permission;
}

export type PermissionPlanConfig = Partial<
  Record<
    Permission['id'],
    {
      limit?: number;
    }
  >
>;

export type PermissionPlan<
  Plan extends Readonly<Permission[]> = Readonly<Permission[]>,
  Config extends PermissionPlanConfig = PermissionPlanConfig
> = {
  [Permission in Plan[number] as Permission['id']]: Permission['config'] & Config[Permission['id']];
};
