import { createPermissionPlan } from '../generator';
import {
  ProjectCollaboratorsPermission,
  MyProjectPermission,
  MyProjectVersionPermission,
  MyProjectVersionSectionPermission,
  ProjectVersionResponsePermission
} from './permissions';

const allPermissions = [
  ...Object.values(MyProjectPermission),
  ...Object.values(MyProjectVersionPermission),
  ...Object.values(MyProjectVersionSectionPermission),
  ...Object.values(ProjectVersionResponsePermission),
  ...Object.values(ProjectCollaboratorsPermission)
];

export enum PermissionPlans {
  Internal = 'internal',
  IndividualFree = 'individualFree',
  IndividualBasic = 'individualBasic',
  IndividualPro = 'individualPro'
}

export const permissionPlans = {
  [PermissionPlans.IndividualFree]: createPermissionPlan(allPermissions, {
    [MyProjectPermission.create.id]: { limit: 1 },
    [MyProjectVersionPermission.create.id]: { limit: 1 },
    [MyProjectVersionSectionPermission.create.id]: { limit: 7 },
    [ProjectVersionResponsePermission.create.id]: { limit: 5 },
    [ProjectCollaboratorsPermission.create.id]: { limit: 1 }
  }),
  [PermissionPlans.IndividualBasic]: createPermissionPlan(allPermissions, {
    [MyProjectPermission.create.id]: { limit: 5 },
    [MyProjectVersionPermission.create.id]: { limit: 3 },
    [MyProjectVersionSectionPermission.create.id]: { limit: 12 },
    [ProjectVersionResponsePermission.create.id]: { limit: 25 },
    [ProjectCollaboratorsPermission.create.id]: { limit: 3 }
  }),
  [PermissionPlans.IndividualPro]: createPermissionPlan(allPermissions),
  [PermissionPlans.Internal]: createPermissionPlan(allPermissions)
};
