import { createPermissionSet } from '../generator';

export const MyProjectPermission = createPermissionSet(1, {});
export const MyProjectVersionPermission = createPermissionSet(2, {});
export const MyProjectVersionSectionPermission = createPermissionSet(3, {});
export const ProjectVersionResponsePermission = createPermissionSet(4, {});
export const ProjectCollaboratorsPermission = createPermissionSet(5, {});
