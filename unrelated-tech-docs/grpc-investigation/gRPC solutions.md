# 1. Direct communication: App UI `<-gRPC->` PCS server

---

| PROS                                                                     | CONS                                 |
| ------------------------------------------------------------------------ | ------------------------------------ |
| minimum latency                                                          | requires UI implementation           |
| one-off implementation that can be used by all sports in App hassle-free | requires mocking of the gRPC service |
| no middleman                                                             | possibly hard to test                |

---

</br>
</br>
</br>
-----
# 2. Indirect communication: App UI `<-Grapqhl->` SL `<-gRPC->` PCS server

---

| PROS                                                                                              | CONS                                                                                                                                               |
| ------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| mocking of SL already in place                                                                    | gRPC integration requires duplication per sport (we have separate SLs for each sport)                                                              |
| easier migration (from MPM to SL than from MPM to PCS)                                            | PCS deploys might require syncing with all SLs as well from time to time (once reaching a stable build version, these cases should be rare though) |
| no new dependencies (gRPC-web library is not needed)                                              | gRPC low latency benefit is lost (higher response times due to having HTTP/1 communication between App UI & SL)                                    |
| interaction with a single API (is the responsibility of the SL to provide both game & event data) |                                                                                                                                                    |

---

</br>
</br>
</br>
</br>
</br>
</br>

---

# For direct communication:

### 1. Proxy server (standard config)

---

| PROS                                                                        | CONS                                                                     |
| --------------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| makes for an easy integration of the gRPC protocol between client & service | another tool to manage on the long term                                  |
| can help with mocking gRPC functionality                                    | additional infrastructure requirements (one setup for every environment) |
|                                                                             | additional project dependency                                            |
|                                                                             | additional tooling needed for the local environment (docker)             |

---

### 2. PCS `gRPC-web` support (possible for some of the languages: Java, .Net)

---

| PROS                                                            | CONS                                        |
| --------------------------------------------------------------- | ------------------------------------------- |
| UI gRPC integration (gRPC-web) works with no additional tooling | management of the libraries on the PCS side |
| gRPC service supports all web apps that implement gRPC-web      | proxy server for mocking                    |

---
