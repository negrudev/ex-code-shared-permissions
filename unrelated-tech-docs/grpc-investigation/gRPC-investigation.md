# Investigation - App UI gRPC integration

In gRPC, a client application can directly call a method on a server application on a different machine as if it were a local object, making it easier for you to create distributed applications and services.

The basic idea is to use remote functionality like it was locally scoped.

This technology's development is still in progress, but packages are made available in order to integrate gRPC in most platforms.

- For software services, `gRPC` protocol can be implemented in multiple programming languages
- For browsers, `gRPC-web` protocol is available, and in order to communicate with a `gRPC` service needs proxying (a translator from `gRPC-web` to `gRPC HTTP/2`, commonly reffered to as simply `gRPC`)

The current App UI application context unveils 2 different approaches, each with its specifics:

### 1. Complete gRPC communication: App UI <-gRPC-> PCS server

| PROS                                                                     | CONS                                 |
| ------------------------------------------------------------------------ | ------------------------------------ |
| minimum latency                                                          | requires UI implementation           |
| one-off implementation that can be used by all sports in App hassle-free | requires mocking of the gRPC service |
| no middleman                                                             | possibly hard to test                |

At this stage, the integration of a `browser gRPC client` with a `gRPC service` requires one of the following approaches:

1. Proxy server
   This implies that between the browser client and the gRPC service there will be a separate service available at a certain endpoint which will forward requests from one to the other (from client to the service or the other way around).

The requirements are as follows:

- the clients uses `grpc-web` library in order to use the `gRPC-web` protocol
- the `gRPC service` is made available to other gRPC clients
- a proxy server which maps the `gRPC-web` requests to the `gRPC` compliant form and forwards the requests/responses

| PROS                                                                        | CONS                                                                     |
| --------------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| makes for an easy integration of the gRPC protocol between client & service | another tool to manage on the long term                                  |
| can help with mocking gRPC functionality                                    | additional infrastructure requirements (one setup for every environment) |
|                                                                             | additional project dependency                                            |
|                                                                             | additional tooling needed for the local environment (docker)             |

2. In-process service support for the `gRPC-web` protocol (possible for some of the languages: Java, .Net)

- for example, for Java, a `Java jar` file is made available which needs to be added to the Java service build. By using it, the gRPC service can support both type of clients: `gRPC` & `gRPC-web`
- this means the browser client can use the `gRPC-web` protocol in order to communicate directly with the gRPC service.

| PROS                                                            | CONS                                        |
| --------------------------------------------------------------- | ------------------------------------------- |
| UI gRPC integration (gRPC-web) works with no additional tooling | management of the libraries on the PCS side |
| gRPC service supports all web apps that implement gRPC-web      | difficulty in mocking the service           |

### 2. Partial gRPC communication: App UI <-Grapqhl-> SL <-gRPC-> PCS server

| PROS                                                                                              | CONS                                                                                                                                               |
| ------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| mocking of SL already in place                                                                    | gRPC integration requires duplication per sport (we have separate SLs for each sport)                                                              |
| easier migration (from MPM to SL than from MPM to PCS)                                            | PCS deploys might require syncing with all SLs as well from time to time (once reaching a stable build version, these cases should be rare though) |
| no new dependencies (gRPC-web library is not needed)                                              | gRPC low latency benefit is lost (higher response times due to having HTTP/1 communication between App UI & SL)                                    |
| interaction with a single API (is the responsibility of the SL to provide both game & event data) |                                                                                                                                                    |

This implies that the SL will act as a middleman between the UI & PCS. It will implement the `gRPC` protocol and the communication with PCS would be its responsibility (this process is pretty straightforward) while the UI will be able to consume updates or initiate request using the mechanism already in place.
At this point, the only API with which the UI will have to communicate would be the SL.

# Demo

A demo was made available in order to test the approach presented at #1 (Complete gRPC communication) where the client (UI) talks directly to the service (PCS).

The demo implementation contains:

- [UI implementation example](https://github.com/...)
- UI tests example (in order to see how complicated mocking gRPC would be)
- [UI mock server implementation](https://github.com/...):
  - mock server code (nodeJS)
  - local `Envoy` proxy server (run in docker) - this is mandatory for the mock server (with or without PCS implementing `gRPC-web` protocol)

## Setup - this would be standard setup for gRPC integration on the UI side

1. We install the required libraries by running the following:

```
npm i @grpc/grpc-js @grpc/proto-loader
```

2. We define the type of request data, response data and the type of rpc call (protobuf definitions) in a `*.proto` file (ex: `data.proto` on the mock server side & `event.proto` on the client side) on both the mock server & UI application.

ex:

```
syntax = "proto3";

message TradeEvent {
  uint32 id = 1;
  bool suspended = 2;
}

message EventsList {
   repeated TradeEvent events = 1;
}

message Empty {}

message GetEventRequest {
  uint32 id = 1;
}

message UpdateEventRequest {
  uint32 id = 1;
  bool suspended = 2;
}

service EventService {
    rpc GetAllEvents (Empty) returns (EventsList) {}
    rpc GetEvent (GetEventRequest) returns (TradeEvent) {}
    rpc UpdateEvent (UpdateEventRequest) returns (Empty) {}
}
```

3. We initialize the mock server and loat the protobuf definitions.

```
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

const PROTO_DEFINITION_PATH = require('path').resolve(__dirname, './proto/data.proto')
const packageDefinition = protoLoader.loadSync(PROTO_DEFINITION_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
});
const eventProto = grpc.loadPackageDefinition(packageDefinition);

const grpcServer = new grpc.Server();

```

4. The mock server defines the type of gRPC requests it can handle and what operations/responses correspond to that.

```
grpcServer.addService(eventProto.EventService.service, {
    getEvent: (call, callback) => {
        callback(null, events.event.find(event => event.id === call.request.id))
    },
    updateEvent: (call, callback) => {
        console.log('@ will update Event', call.request);
        callback(null, {})
    },
    getAllEvents: (call, callback) => {
        const eventIds = events.event.map(event => ({ id: event.id, suspended: event.suspended }))
        callback(null, { events: eventIds })
    },
});
```

5. As the mock nodeJS server now supports `gRPC`, the next step is to add a proxy that will translate requests from `gRPC-web` (the protocol used by the client app) to `gRPC`.
   We do this by creating an `envoy.yaml` file at the root of the mock server, and creating a docker container out of the `envoyproxy/envoy` docker image.

Install docker & run the following command at the root of the mock server:

on Windows:

```
$ docker run -d -v "$(pwd)"/envoy.yaml:/etc/envoy/envoy.yaml:ro --network=host envoyproxy/envoy:v1.17.0
```

on MacOS:

```
docker run -d -v "$(pwd)"/envoy.yaml:/etc/envoy/envoy.yaml:ro -p 8080:8080 -p 9901:9901 envoyproxy/envoy:v1.17.0
```

6. At this stage the `Envoy` proxy server should be up and running. Next step is to integrate the `gRPC-web` in the UI app.
   For this, we need to install the following libraries:

```
npm i grpc-web google-protobuf
```

7. Install the `protoc` protocol buffer compiler in order to generate the required client JS code from the `*.proto` file.

Instructions: https://grpc.io/docs/protoc-installation/

8. Generate the client JS code from the `*.proto` file in order to be able to call RPC methods with the help of `protoc`.
   Run this on the `*.proto` file (within the context of the demo, the proto file is `src/modules/common/api/eventServiceGRPC/event.proto`)

```
protoc -I=. src/modules/common/api/eventServiceGRPC/event.proto --js_out=import_style=commonjs:. --grpc-web_out=import_style=typescript,mode=grpcwebtext:.
```

This will have generated js classess/methods/typescript definition files for all the rpc methods & variables.

In the context of the demo, the generated files were: `event_pb.js`, `event_pb.d.ts`, `EventServiceClientPb.ts`.

9. Initialize the client gRPC object

```
const grpcServiceEndpoint = 'http://' + window.location.hostname + ':8080';
const serviceInst = new EventServiceClient(grpcServiceEndpoint, null, null);
```

10. Final step: import and use RPC methods defined in the `*.proto` file

Every method needs a request payload of the type defined in the `*.proto` file. We do this by instantiating an object of the request class type (ex: `new GetEventRequest()`). In order to add data to the request, we use the method exposed by the request object.

```
const request = new GetEventRequest();
request.setId(eventId);
```

All methods have a callback function which handles the response or error. Every response comes in the form of a response object defined in the `*.proto` file. This object exposes specific methods in order to extract information from it (ex: `toObject`).

```
(err, response) => {
    console.log(response.toObject());
});
```

In the context of the demo, the end result looks like the following (promises are used in order to avoid callback hell):

```
import { EventServiceClient } from './EventServiceClientPb';
import { GetEventRequest, UpdateEventRequest, Empty } from './event_pb';

export const eventGet = (eventId: number): Promise<unknown> => {
    //  Create request payload
    const request = new GetEventRequest();
    request.setId(eventId);

    return new Promise((resolve, reject) => {
        // Call GetEvent rpc method
        serviceInst.getEvent(request, {}, (err, response) => {
            if(err) reject(err);
            resolve(response.toObject());
        });
    });
};

const event = await eventGet(29229608);
```

## Mock server

The current App UI mock server needs an `Envoy` proxy in order to be able to handle `gRPC-web` requests. This is the case because currently there is no `gRPC-web` library/package for the nodeJS runtime. (there is for Java and .Net)
There is an overhead associated with the `Envoy` proxy but the configuration is pretty standard & straight forward. However, issues may arise when configuring it for the first time (port forwarding issues, docker issues)
With this setup, we are able to easily mock the entire gRPC functionality and send back any response we deem appropriate.

## Client side

The cliend side flow is pretty simple:

- we have the same `*.proto` file as on the server
- we run a utility service (`protoc`) in order to generate classess/methods/types out of the `*.proto` file
- we use the methods generated as like it were locally scoped

## Testing

At this stage it is pretty easy and straight forward to test any gRPC related functionality.

In the context of the demo, we have a function with a dependency on a gRPC method, which was easily mocked with the help of jest.

```
const mockEvent = {
    id: 'mock',
    suspended: true
};

const fnWithGrpcDependency = async () => {
    const event = await eventGet(29229608);
    return event;
};

it('should get event', async () => {
    //  Mock gRPC related functionality
    jest.spyOn(eventService, 'eventGet').mockImplementation(() => new Promise((resolve) => resolve(mockEvent)));

    //  Check if gRPC dependency was successfully mocked
    const result = await fnWithGrpcDependency();
    expect(result).toEqual(mockEvent);
});
```

## Possible issues:

- type mismatch between what we expect in the codebase and what we defined in the `*.proto` file
- not creating a gRPC request of a class type. We must alwasy create a request object of a protobuf class type, even if empty (demo ex: `new Empty()`)
- array type in a gRPC response must be sent under the proper key (demo ex: `callback(null, { events: eventIds })`)
- port forwarding issues in the `Envoy` configuration
- docker issues
